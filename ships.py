from colors import * ;
from functions import *;
import main;


class Ship(main.MainGame):
	"""docstring for ship"""
	def __init__(self):
		super(Ship,self).__init__();
		self.armed = True;
		self.pos = [350,250];
		self.color = (200,200,200);
		self.b2 = [0,1];
		self.b1 = [1,0];
		self.moveforward = False;
		self.rot_left=False;
		self.rot_right=False;
		self.rot_step=.1;
		self.life = 100;
		self.impulse_vel = 5;
		self.boost = 10;
		self.vel = self.impulse_vel;
		self.h = 70;
		self.w = 30;
		self.slow_down=False;
		self.center = self.coo(self.w/2, self.h/2);
		self.photons = 0;
		self.torpedos = 0;
		self.font = pygame.font.SysFont("None",20);
		self.shield =100;
		self.shielded = False;


	def rotate(self,angle):
		self.b1 = rotate(self.b1,angle);
		self.b2 = rotate(self.b2,angle);
	def draw_bars(self):
		pygame.draw.rect(self.screen,YELLOW,[10,10,100,10],5);
		pygame.draw.rect(self.screen,BLACK,[10,10,100,10]);
		pygame.draw.rect(self.screen,GREEN,[10,10,self.life,10]);
		if self.shielded and self.shield>=0:
			self.shield-=.5;
		else:
			if self.shield<=100:
				self.shield+=.1;
		pygame.draw.rect(self.screen,YELLOW,[10,25,100,13],5);
		pygame.draw.rect(self.screen,BLACK,[10,25,100,13]);
		pygame.draw.rect(self.screen,BLUE,[10,25,self.shield,13]);
		shield = self.font.render("shield: "+str(int(self.shield))+"%",0,WHITE);
		self.screen.blit(shield,(20,25));

		photons = self.font.render("Photons: "+str(self.photons),0,RED);
		self.screen.blit(photons,(20,40));
		torpedos = self.font.render("Torpedos: "+str(self.torpedos),0,RED);
		self.screen.blit(torpedos,(20,52));
	def coo(self,x,y,limit = True ):
		if limit:
			if x >= self.w:
				x = self.w;
			if y >= self.h:
				y = self.h;
		return lint(lsum( lsum(lprod(x-self.w/2,self.b1),lprod(y-self.h/2,self.b2)) ,self.pos));
	def reset_center(self):
		self.center = self.coo(self.w/2, self.h/2);
	def move_forward(self):
		self.pos=lsub(self.pos,lprod(self.vel,self.b2));
		self.reset_center();
	def draw_ship(self):		
		#circle
		pygame.draw.circle(self.screen,self.color,lint(self.coo(self.w/2, self.w/2))	,self.w/2);
		#neck
		pygame.draw.line(self.screen,self.color, self.coo(self.w/2,self.w/2),self.coo(self.w/2,self.w/2+40),5)
		# left  propeller connectiom
		pygame.draw.line(self.screen,self.color, self.coo(self.w/2-3,self.w/2+35),self.coo(self.w/7, self.h/1.8),3);
		#left propeller
		pygame.draw.line(self.screen,self.color, self.coo(self.w/7, self.h/1.8-4),self.coo(self.w/7, self.h/1.8-4+30),4);
		#right propeller connectiom	
		pygame.draw.line(self.screen,self.color, self.coo(self.w/2+2,self.w/2+35),self.coo(6*self.w/7, self.h/1.8),3);
		#right propeller
		pygame.draw.line(self.screen,self.color, self.coo(6*self.w/7, self.h/1.8-4),self.coo(6*self.w/7, self.h/1.8-4+30),4);
		#dimension markers
		"""
		pygame.draw.circle(self.screen,RED,self.coo(0,0),3);
		pygame.draw.circle(self.screen,RED,self.coo(self.w,self.h),3);
		pygame.draw.circle(self.screen,RED,self.coo(0,self.h),3);
		pygame.draw.circle(self.screen,RED,self.coo(self.w,0),3);
		"""
	def random_points(self, n=10):
		plist = [];
		for i in range(n):
			p = rand_pos([self.w,self.h]);
			plist+=[self.coo(p[0],p[1] )];
		return plist;
	def draw(self):		
		self.draw_bars();
		#SLOWING DOWN 
		if self.rot_right:
			self.rotate(self.rot_step);
		if self.rot_left:
			self.rotate(-self.rot_step);
		if self.moveforward:
			self.move_forward();
			pygame.draw.line(self.screen,YELLOW, self.coo(self.w/7, self.h/1.8),self.coo(self.w/7, self.h/1.8-1+self.vel*30),6);
			pygame.draw.line(self.screen,YELLOW, self.coo(6*self.w/7, self.h/1.8),self.coo(6*self.w/7, self.h/1.8-1+self.vel*30),6);
		else:
			if self.slow_down:
				if self.vel >= .1:
					self.vel -=.05;
					self.move_forward();
				else:
					self.slow_down=False;
					self.vel=self.impulse_vel;
		self.draw_ship();
		if self.shielded and self.shield>=0:
			[pygame.draw.circle(self.screen,BLUE, p,1) for p in self.random_points(n=100)];






