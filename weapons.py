from colors import * 
from functions import * 
import main;


class Projectile(main.MainGame):
	"""docstring for Projectile"""
	def __init__(self,Owner):
		super(Projectile,self).__init__();
		self.velmod = 6;
		self.color = GREEN;
		self.dimension = 2;
		self.probabilty = 0.01;
		self.damage = 5;
		self.armed = True;
		self.owner = Owner;
		self.pos = 	[0,0];
		self.vel=[0,0]
		self.nvel = [0,0];		
		self.fired = False;		
		self.sound=[];
	def fire(self,ipos,ivel):
		self.fired=True;
		self.nvel = lrescale(ivel,k=self.velmod);
		self.pos = ipos;
		if len(self.sound)!=0:
			pygame.mixer.Sound(self.sound).play();
	def move_forward(self):
		if self.fired:
			self.pos = lint(lsum(self.pos,self.nvel));
	def reset(self):
		self.fired = False;
	def disarm(self):
		self.armed=False;
	def isOutside(self):
		if self.pos[0]>=self.screen_size[0] or self.pos[1]>=self.screen_size[1] or self.pos[0]<=0 or self.pos[1] <=0:
			return True;
		else:
			return False;
	def draw(self):
		if self.fired:
			pygame.draw.circle(self.screen,self.color,self.pos,self.dimension);
class Photon(Projectile):
	def __init__(self,Owner):
		super(Photon, self).__init__(Owner);
		self.probabilty=0.001;
		self.velmod = 6;
		self.dimension=2;
		self.color = GREEN;
		self.damage = 5;
		self.sound="sounds/laser5.ogg";
class Torpedo(Projectile):
	def __init__(self,Owner):
		super(Torpedo, self).__init__(Owner);
		self.probabilty=0.001;
		self.velmod = 3;
		self.dimension=5;
		self.color = RED;
		self.damage = 20;
		self.sound ="sounds/laser-02.ogg";



