from header import *;

#custom libraries
from colors import * ;
from functions import *; 
import enemies;
import weapons;
import ships;
import main;

pygame.init();


#NON-BOOLEAN PREDEFINITIONS 
maingame = main.MainGame();

SIZE =maingame.screen_size;
screen = maingame.screen;
pygame.display.set_caption("Nango's Space!")
clock = pygame.time.Clock()


starpos = stars_position(SIZE,stars = 100);



#FONTS ---------------
main_font = pygame.font.SysFont("None",60);
life_font = pygame.font.SysFont("None",20);





#SOUNDS---------------
pygame.mixer.Sound("sounds/opening.ogg").play();


Game_is_over = False;

for level in range(1,10):
	#BOOLEAN PREDEFINITIONS --------
	start = True;
	youwin = False;
	done= True;
	no_collision=False;
	miliseconds=0;
	if Game_is_over:
		break;
	#OBJETS PREDEFINITIONS---------------	
	print("LEVEL %d"%level);
	#ENEMIES CREATION-------------
	number_of_enemies=level;
	kill_count = 0;
	enemies_list=[];
	enemies_projectile_list = [];
	for i in range(number_of_enemies):
		new_enemy = enemies.BorgCube([1,1]);
		new_enemy.dimension = 70;
		new_proj = weapons.Projectile(new_enemy);
		new_tor = weapons.Torpedo(new_enemy);
		enemies_list+=[new_enemy];
		enemies_projectile_list+=[new_proj,new_tor];
	enterprise = ships.Ship();
	ship_projectile_list = [];
	enterprise.photons = number_of_enemies*20;
	enterprise.torpedos = number_of_enemies*2;
	for i in range(enterprise.photons):
		new_proj = weapons.Photon(enterprise);
		ship_projectile_list+=[new_proj];
	for i in range(enterprise.torpedos):
		new_proj = weapons.Torpedo(enterprise);
		ship_projectile_list+=[new_proj];

	while done:
		for event in pygame.event.get():
			print event;
			if event.type == pygame.KEYDOWN:
				if event.key==27:
					done=False;
					Game_is_over=True;
				if event.key==pygame.K_UP:#up
					enterprise.vel = enterprise.impulse_vel;
					enterprise.slow_down=False;
					enterprise.moveforward=True;
				if event.key==pygame.K_LEFT:#left
					enterprise.rot_left=True;
				if event.key==pygame.K_RIGHT:#right
					enterprise.rot_right=True;
				if event.key==pygame.K_SPACE: #space
					if enterprise.armed:
						print("Armed!")
						for proj in ship_projectile_list:
							if type(proj) is weapons.Photon:
								if proj.armed and not proj.fired:
									print("CAPTAIN:    FIRE LASERS!!!!!!")
									proj.fire(enterprise.center,lprod(-1,enterprise.b2));
									enterprise.photons-=1;
									break;
				if event.key ==pygame.K_b:#b
					enterprise.slow_down=False;
					enterprise.vel=enterprise.boost;
				if event.key ==pygame.K_t:#t
					if enterprise.armed:
						print("Armed!")
						for proj in ship_projectile_list:
							if type(proj) is weapons.Torpedo:
								if proj.armed and not proj.fired:
									print("CAPTAIN:    FIRE TORPEDOS!!!!")
									proj.fire(enterprise.center,lprod(-1,enterprise.b2));
									enterprise.torpedos-=1;
									break;
				if event.key == pygame.K_s:
					if enterprise.shield>=0:
						enterprise.shielded=True;
			if event.type==pygame.KEYUP:
				if event.key==273:#up
					enterprise.moveforward=False;
					enterprise.slow_down = True;
				if event.key==276:#left
					enterprise.rot_left=False;
				if event.key==275:#right
					enterprise.rot_right=False;
				if event.key ==98:#b
					enterprise.vel=enterprise.impulse_vel;
				if event.key == pygame.K_s:
					enterprise.shielded=False;
		screen.fill(BLACK);
		#THINGS TO DO at START
		if start:
			enterprise.shielded=True;
			enterprise.shield = 100;
			if miliseconds< 50:
				start_text = main_font.render("Destroy the Borg Cube!",0,GREEN);
				screen.blit(start_text,(100,100));
			elif miliseconds>=55:
				start_text = main_font.render("LEVEL %d"%level,0,GREEN);
				screen.blit(start_text,(100,100));
			if miliseconds>150:
				start = False;
				enterprise.shielded=False;
				enterprise.shield=100;
			print miliseconds;
			miliseconds += 1;
		#ARE THERE ANY ENEMIES LEFT?
		if len(enemies_list)==0:
			start_text = main_font.render("ALL IS OK!",0,GREEN);
			screen.blit(start_text,(100,100));
			done = False;
		#DRAW STUFF
		draw_stars(starpos,screen);
	
		if no_collision:
			miliseconds+=1;
			if miliseconds>=no_collision_time:
				no_collision=False;
				enterprise.color = GRAY;
	
		for j,enemy in enumerate(enemies_list):
			enemy.draw(life_font);
			enemy.move_forward();
			#COLLISION SHIP-ENEMY
			if collision_Ship_Borg(enterprise,enemy,n=20,thr=5) and not no_collision and not enterprise.shielded:
				print("COLLISION CAPITAN!!");
				enemy.vel = vel_change(enemy.vel);
				enterprise.life -=10;
				enemy.life -=5;
				if enemy.life <=0:
					enemy.delete();
					enemies_list.remove(enemy);
					print("DESTROYED!!");
				elif enterprise.life <=0:
					enterprise.color = RED;
					done = False;
					print("YOU FUCKING LOSE!!");
				else:
					no_collision = True;
					no_collision_time = 50;
					miliseconds=0;
					enterprise.color = RED;
			#COLLISION PROJECTILES
			for proj in ship_projectile_list:
				if  proj.fired:
					if proj_borg_collision(proj,enemy):
						enemy.life -=proj.damage;
						#IS someone dead yet? 
						if enemy.life <=0:
							enemy.delete();
							enemies_list.remove(enemy);
							print("DESTROYED!!");
						print "GOT IT!";
						proj.reset();
						proj.disarm();
					elif proj.isOutside():
						print("Out!");
						proj.reset();
						proj.disarm();
					else:
						proj.move_forward();
						proj.draw();
		for proj in enemies_projectile_list:
			#print len(enemies_projectile_list);	
			if proj.fired:
				if proj.isOutside():
					print("Out!");
					proj.reset();
				elif proj_ship_collision(proj,enterprise,thr=5,n=20) and not enterprise.shielded:
					print("We have been hit!!!!!----------"*2);
					enterprise.life-=proj.damage;
					proj.reset();
				else:
					proj.move_forward();
					proj.draw();
			else:
				if proj.owner.life >0:
					if random.random()<=proj.probabilty:
						print "Phaser fired!!!";
						enemy = proj.owner;
						proj.fire(enemy.center,lsub(enterprise.pos,enemy.center));
		enterprise.draw();	
		pygame.display.flip();
		clock.tick(40);
pygame.quit();



