from header import * 
from colors import * 

def vel_change(vel):
	return [-vel[1],vel[0]];
def collision_Ship_Borg(ship,borg,thr=5,n=20):
	#if (borg.pos[0]<=ship.pos[0]+ship.w<= borg.pos[0]+borg.dimension) and  (borg.pos[1]<=ship.pos[1]+ship.h<= borg.pos[1]+borg.dimension):
	#	return True;
	rborgpoints = borg.random_points(n);
	rshippoints = ship.random_points(n);
	for i in range(n):
		for j in range(n):
			if abs(lnorm(lsub(rborgpoints[i],rshippoints[j])))<=thr:
				return True;
	return False;

def proj_borg_collision(proj,borg):
	if (borg.pos[0]<=proj.pos[0]<= borg.pos[0]+borg.dimension) and  (borg.pos[1]<=proj.pos[1]<= borg.pos[1]+borg.dimension):
		return True;
def proj_ship_collision(proj,ship,thr=5,n=20):
	rpoitns = ship.random_points(n);
	ppos = proj.pos;
	minl = min([lnorm(lsub(p,ppos)) for p in rpoitns]); 
	if minl<=thr:
		return True;
	else:
		return False;
def rotate(pos,angle):
	return [pos[0]*math.cos(angle)-pos[1]*math.sin(angle), pos[0]*math.sin(angle) + pos[1]*math.cos(angle) ];
def lint(l1):
	return [int(p) for p in l1];
def lnorm(l1):
	return math.sqrt(sum([p**2 for p in l1]));
def lprod(number,l1):
	return [number*p for p in l1];
def lsum(l1,l2):
    return [l1[0]+l2[0],l1[1]+l2[1]];
def lsub(l1,l2):
    return [l1[0]-l2[0],l1[1]-l2[1]];
def rand_pos(SIZE):
	return [int(SIZE[0]*random.random()), int(SIZE[1]*random.random())];
def stars_position(SIZE, stars = 100):
	return [rand_pos(SIZE) for i in range(stars)];
def draw_stars(poslist,screen):
	for pos in poslist:
		pygame.draw.circle(screen,WHITE,pos,2);
def lrescale(l1,k=1):
	return lprod(k*lnorm(l1)**(-1),l1);






