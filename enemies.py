from colors import * 
from functions import * 
import main;

class BorgCube(main.MainGame):
	"""docstring for Projectile"""
	def __init__(self,ivel):
		super(BorgCube,self).__init__();
		self.color = GREEN;
		self.vel = ivel;
		self.dimension = 100;
		self.life = 100;
		self.pos = 	rand_pos(lsum(self.screen_size,[-self.dimension,-self.dimension]));
		self.center = lsum(self.pos,[self.dimension/2,self.dimension/2]);
	def reset_center(self, pos):
		self.center = lsum(self.pos,[self.dimension/2,self.dimension/2]);
	def draw_text(self,text,font):
		retext = font.render(text,0,RED);
		self.screen.blit(retext,lsum(self.pos,[-len(text),-15]));
	def move_forward(self):
		if self.pos[0]<=0 or self.pos[1]<=0:
			self.vel = [-self.vel[1],self.vel[0]];
		if self.pos[0]>=self.screen_size[0]-self.dimension or self.pos[1]>=self.screen_size[1]-self.dimension:
			self.vel = [-self.vel[1],self.vel[0]];
		self.pos = lint(lsum(self.pos,self.vel));
		self.reset_center(self.pos);
	def draw_life(self):
		pygame.draw.rect(self.screen,GREEN,lsum(self.pos,[0,self.dimension+5])+[100,5],2);
		pygame.draw.rect(self.screen,BLACK,lsum(self.pos,[0,self.dimension+5])+[100,5]);
		pygame.draw.rect(self.screen,RED,lsum(self.pos,[0,self.dimension+5])+[self.life,5]);
	def random_points(self,n=10):
		rpoints = [];
		for i in range(n):
			rpoint = rand_pos([self.dimension,self.dimension]);
			rpoint = lsum(self.pos, rpoint);
			rpoints +=[rpoint];
		return rpoints;
	def draw(self,font):
		pygame.draw.rect(self.screen,self.color,self.pos+[self.dimension,self.dimension],3);
		pygame.draw.rect(self.screen,BLACK,self.pos+[self.dimension,self.dimension]);
		for i in range(0,10):
			pygame.draw.line(self.screen,self.color,lsum(self.pos,[self.dimension/10*i,0]),lsum(self.pos,[self.dimension/10*i,self.dimension]));
			pygame.draw.line(self.screen,self.color,lsum(self.pos,[0,self.dimension/10*i]),lsum(self.pos,[self.dimension, self.dimension/10*i]))
		if self.pos[0]<=self.screen_size[0]/2 and self.pos[1]<=self.screen_size[1]/2:
			self.draw_text("WE ARE THE BORG!",font);	
		if self.pos[0]>self.screen_size[0]/2 and self.pos[1]>self.screen_size[1]/2:
			self.draw_text("RESISTANCE IS FUTILE!",font);	
		self.draw_life();
		#print self.center;
		#pygame.draw.circle(self.screen, BLUE, lsum(self.pos,[self.dimension/2,self.dimension/2]),5);
		#for p in self.random_points(n=100):
		#	pygame.draw.circle(self.screen,BLUE,p,1);



